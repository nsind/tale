# Quelques liens

## Autres enseignements

- Le dépôt GIT des cours de [NSI](https://gitlab.com/lyceeND/)
-  Un [ancien  site](https://informathix.tuxfamily.org/)  avec  des cours  d'IUT
  Info, d'info en MPSI/MP*, de programmation en divers langages et bien d'autres choses.
- Le dépôt GIΤ  du [DU NSI](https://gitlab.com/GiYoM/du/-/tree/master/) que j'ai
  animé à l'UCO.
- Le dépôt GIT du cours de [Python en L1](https://github.com/Informathix/Complements_Info_L1_UCO_Angers)
  en [L2](https://github.com/Informathix/UCO_L2) et en [L3](https://github.com/Informathix/UCO_L3)


